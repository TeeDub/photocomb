[reflection.assembly]::loadfile( "C:\Windows\Microsoft.NET\Framework\v2.0.50727\System.Drawing.dll")

$SourceDrive = "p:"
$SourcePath = "Incoming"
$TargetDrive = "p:"
$TargetPath = "PowerShell-Sorted"
$DupeDrive = "p:"
$DupePath = "Duplicates"
$LogDrive = "p:"
$LogPath = "Logs"
$global:Inc = 1
$Counter = 0
$global:arrHash = @()
$global:DupeLog = $LogDrive + "\" + $LogPath + "\dupelog.txt"
$global:HashLog = $LogDrive + "\" + $LogPath + "\hashlog.txt"

function xCopyFile($FileFullName, $DestDir, $DestFileFull, $FileShortName) {
	$TestFileHash = get-filehash -Algorithm MD5 $FileFullName|foreach { $_.Hash }
	If ($global:arrHash -contains $TestFileHash) {
		Add-Content $global:DupeLog $FileFullName
		If (Test-Path $DupeDrive\$DupePath\$FileShortName) {
			$newFileName = [string]$global:inc + "_" + $FileShortName
			$global:inc++
			Move-Item $FileFullName $DupeDrive\$DupePath\$newFileName
        	}
		Else {
			Move-Item $FileFullName $DupeDrive\$DupePath\$FileShortName
			}
		return 1
		}
	If (Test-Path $DestFileFull) {
		$newFileName = [string]$global:inc + "_" + $FileShortName
		$global:inc++
        }
	Else {
		$newFileName = $FileShortName
		}
	$NewFull = $DestDir + "\" + $newFileName
	TestDestination $DestDir
	Move-Item $FileFullName $NewFull
    $NewHash = get-filehash -Algorithm MD5 $NewFull|foreach { $_.Hash }
	$global:arrHash += $NewHash
    add-content $global:HashLog $NewHash
	}

function TestDestination($DestinationDir) {
	If (!(Test-Path -Path $DestinationDir)) {
		new-item $DestinationDir -Type Directory > $null
  		}
    }

function DateTakenProperty($date) {
	#each character represents an ascii code number 0-10 is date
	#10th character is space separator between date and time
	#48 = 0 49 = 1 50 = 2 51 = 3 52 = 4 53 = 5 54 = 6 55 = 7 56 = 8 57 = 9 58 = :
	#date is in YYYY/MM/DD format
	$arYear = [Char]$date[0],[Char]$date[1],[Char]$date[2],[Char]$date[3]
	$arMonth = [Char]$date[5],[Char]$date[6]
	$arDay = [Char]$date[8],[Char]$date[9]
	$strYear = [String]::Join("",$arYear)
	$strMonth = [String]::Join("",$arMonth)
	$strDay = [String]::Join("",$arDay)
	return $strYear + "-" + $strMonth + "-" + $strDay
	}

If (Test-Path -Path $SourceDrive\SourcePath) {
	Write-Host "Source Directory does not exist!"
	exit 1
	}
TestDestination($TargetDrive + "\" + $TargetPath)
TestDestination($DupeDrive + "\" + $DupePath)
TestDestination($LogDrive + "\" + $LogPath)
If (Test-Path -Path $global:HashLog) {
	$global:arrHash += Get-Content $global:HashLog
	}
$Files = Get-ChildItem -recurse -include @("*.tif","*.jpg","*.jpeg","*.bmp","*.gif","*.png") $SourceDrive\$SourcePath
foreach ($file in $Files) {
	$foo = New-Object -TypeName system.drawing.bitmap -ArgumentList $file.fullname -ErrorAction "SilentlyContinue"
    If ($foo.PropertyIdList -contains 36867) {
		$DateTaken = DateTakenProperty($foo.GetPropertyItem(36867).value[0..9])
		$TargetLocation = $TargetDrive + "\" + $TargetPath + "\Photos\" + $DateTaken.Substring(0,4) + "\" + $DateTaken
		}
	Else {
		$TargetLocation = $TargetDrive + "\" + $TargetPath + "\Photos\--"
		}
	$foo.Dispose()
	#Remove-Variable foo
	$TargetFile = $TargetLocation + "\" + $file.Name
    TestDestination $TargetLocation
	$xCopyReturn = xCopyFile $file.FullName $TargetLocation $TargetFile $file.name
    $Counter++
	}
$Files = Get-ChildItem -Recurse -Include @("*.avi","*.mov","*.mts","*.mpg","*.wmv","*.vob","*.m4v","*.mp4") $SourceDrive\$SourcePath
foreach ($file in $Files) {
	$FileDate = $file.LastWriteTime
    $DateTaken = $FileDate.ToString("yyyy-MM-dd")
	$TargetLocation = $TargetDrive + "\" + $TargetPath + "\Movies\" + $DateTaken.Substring(0,4) + "\" + $DateTaken
	$TargetFile = $TargetLocation + "\" + $file.Name
    #TestDestination $TargetLocation
	$xCopyReturn = xCopyFile $file.FullName $TargetLocation $TargetFile $file.name
    $Counter++
	}
$Files = Get-ChildItem -Recurse -Include @("*.nef","*.raw","*.psd","*.pcd") $SourceDrive\$SourcePath
foreach ($file in $Files) {
	$TargetLocation = $TargetDrive + "\" + $TargetPath + "\RawImages"
	$TargetFile = $TargetLocation + "\" + $file.Name
    TestDestination $TargetLocation
	$xCopyReturn = xCopyFile $file.FullName $TargetLocation $TargetFile $file.name
    $Counter++
	}
$Files = Get-ChildItem -Recurse -Exclude @("*.dll","*.exe","*.cct","*.db","*.html","*.ithmb","*.txt","*.tmp","*.ini","*.pi2","*.x32","*.avi","*.mov","*.mts","*.mpg","*.wmv","*.vob","*.m4v","*.mp4","*.nef","*.raw","*.psd","*.tif","*.jpg","*.jpeg","*.bmp","*.gif","*.png") $SourceDrive\$SourcePath
foreach ($file in $Files) {
	$TargetLocation = $TargetDrive + "\" + $TargetPath + "\OtherFiles"
	$TargetFile = $TargetLocation + "\" + $file.Name
    TestDestination $TargetLocation
	$xCopyReturn = xCopyFile $file.FullName $TargetLocation $TargetFile $file.name
    $Counter++
	}
#Clean up empty folders from the OtherFiles directory
$OtherFilesDir = $TargetDrive + "\" + $TargetPath + "\OtherFiles"
If (Test-Path -Path $OtherFilesDir) {
	$EmptyFolders = Get-ChildItem -Force -Path $OtherFilesDir -Directory|Where-Object {(Get-ChildItem -Path $_.FullName -Recurse) -eq $null}
	foreach ($d in $EmptyFolders) {
		Remove-Item -Recurse $OtherFilesDir\$d
		}
	}
Clear-Content $global:HashLog
ForEach ($Hash in $global:arrHash) {
	Add-Content $global:HashLog $Hash
	}
Exit 0
